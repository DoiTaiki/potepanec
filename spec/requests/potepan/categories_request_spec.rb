require 'rails_helper'

RSpec.describe "GET /potepan/categories/:id", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:root) { taxonomy.root }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: root) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:image) { create(:image) }

  before do
    product.images << image
    get potepan_category_path(taxon.id)
  end

  it "returns http success" do
    expect(response).to have_http_status(:success)
  end

  it "has correct variables" do
    expect(response.body).to include taxon.name
    expect(response.body).to include product.name
    expect(response.body).to include taxonomy.name
  end
end
