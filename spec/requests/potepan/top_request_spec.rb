require 'rails_helper'

RSpec.describe "GET /top", type: :request do
  it "returns http success" do
    get potepan_top_path
    expect(response).to have_http_status(:success)
  end
end
