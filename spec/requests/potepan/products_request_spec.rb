require 'rails_helper'

RSpec.describe "GET /potepan/products/:id", type: :request do
  let(:taxon) { create(:taxon) }
  let(:image) { create(:image) }
  let(:images) { create_list(:image, 4) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  before do
    product.images << image
    related_products.each_with_index do |related_product, i|
      related_product.images << images[i]
    end
    get potepan_product_path(product.id)
  end

  it "returns http success" do
    expect(response).to have_http_status(:success)
  end

  it "has correct variables" do
    expect(response.body).to include product.name
    expect(response.body).to include product.description
    related_products.each do |related_product|
      expect(response.body).to include related_product.name
    end
  end
end
