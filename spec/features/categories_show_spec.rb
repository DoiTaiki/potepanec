require 'rails_helper'

RSpec.describe "categories/show.html.erb", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxonomy_2) { create(:taxonomy) }
  let(:image) { create(:image) }
  let(:image_2) { create(:image) }
  let(:root) { taxonomy.root }
  let(:root_2) { taxonomy_2.root }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: root, name: "taxon") }
  let(:taxon_2) { create(:taxon, taxonomy: taxonomy_2, parent: root_2, name: "other_taxon") }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:product_2) { create(:product, taxons: [taxon_2], price: 17) }

  before do
    product.images << image
    product_2.images << image_2
    visit potepan_category_path(taxon.id)
  end

  it "has correct title" do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
  end

  it "has 'shop' string in pageHeader" do
    expect(page).to have_content "shop"
  end

  it "has dropdown-menu in header" do
    expect(page).to have_css "ul.dropdown-menu ul.list-unstyled"
  end

  it "has some contents of product" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_selector "img[src$='#{image.attachment(:small)}']"
  end

  it "doesn't have contents of different taxon's product" do
    expect(page).not_to have_content product_2.name
    expect(page).not_to have_content product_2.display_price
    expect(page).not_to have_selector "img[src$='#{image_2.attachment(:small)}']"
  end

  scenario "user clicks product's name" do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  it "has some contents and links of category" do
    within("div.sideBar ul.side-nav") do
      expect(page).to have_content root.name
      expect(page).to have_content taxon.name
      expect(page).to have_content taxon.products.count
      expect(page).to have_content root_2.name
      expect(page).to have_content taxon_2.name
      expect(page).to have_content taxon_2.products.count
    end
  end

  scenario "user clicks category name which links to category list" do
    within("div.sideBar ul.side-nav") do
      click_on taxon_2.name
      expect(current_path).to eq potepan_category_path(taxon_2.id)
    end
  end
end
