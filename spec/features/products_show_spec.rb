require 'rails_helper'

RSpec.describe "products/show.html.erb", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:taxon_2) { create(:taxon) }
  let(:image) { create(:image) }
  let(:related_product_image) { create(:image) }
  let(:unrelated_product_image) { create(:image) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_product) { create(:product, taxons: [taxon]) }
  let!(:other_related_products) { create_list(:product, 3, taxons: [taxon]) }
  let(:unrelated_product) { create(:product, taxons: [taxon_2], price: 17) }

  before do
    product.images << image
    related_product.images << related_product_image
    unrelated_product.images << unrelated_product_image
    visit potepan_product_path(product.id)
  end

  it "has correct title" do
    expect(page).to have_title "#{product.name} - BIGBAG Store"
  end

  it "doesn't have 'shop' string in pageHeader" do
    expect(page).not_to have_content "shop"
  end

  it "doesn't have dropdown-menu in header" do
    expect(page).not_to have_css "ul.dropdown-menu ul.list-unstyled"
  end

  it "has two links to the top page" do
    expect(page).to have_link nil, href: potepan_top_path
    expect(page).to have_link "HOME", href: potepan_top_path
  end

  it "has some contents of product" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    expect(page).to have_selector "img[src$='#{image.attachment(:small)}']"
    expect(page).to have_selector "img[src$='#{image.attachment(:large)}']"
  end

  scenario "user clicks '一覧ページへ戻る' which links to product's category list" do
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  it "has some contents of related_products" do
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.display_price
    expect(page).to have_selector "img[src$='#{related_product_image.attachment(:small)}']"
    expect(page.all("div.productBox").count).to eq 4
  end

  it "doesn't have contents of unrelated product" do
    expect(page).not_to have_content unrelated_product.name
    expect(page).not_to have_content unrelated_product.display_price
    expect(page).not_to have_selector "img[src$='#{unrelated_product_image.attachment(:small)}']"
  end

  scenario "user clicks related_product's name" do
    within("div.productsContent") do
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end
end
