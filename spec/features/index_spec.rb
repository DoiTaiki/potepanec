require 'rails_helper'

RSpec.describe "index.html.erb", type: :feature do
  it "has correct title" do
    visit potepan_top_path
    expect(page).to have_title "BIGBAG Store"
  end
end
