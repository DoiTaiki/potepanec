require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "#related_products" do
    let(:taxon) { create(:taxon) }
    let(:taxon_2) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:related_product) { create(:product, taxons: [taxon]) }
    let(:unrelated_product) { create(:product, taxons: [taxon_2]) }

    it "returns related_product's data and does not returns unrelated_product's data" do
      expect(product.related_products).to match_array [related_product]
    end

    it "returns empty when product is not related with any other products" do
      expect(unrelated_product.related_products).to be_empty
    end
  end
end
