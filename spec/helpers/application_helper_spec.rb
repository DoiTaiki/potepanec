require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe "#full_title" do
    it "returns 'BIGBAG Store' when its argument is empty" do
      expect(full_title(page_title: "")).to eq("BIGBAG Store")
    end

    it "returns 'BIGBAG Store' when its argument is nil" do
      expect(full_title(page_title: nil)).to eq("BIGBAG Store")
    end

    it "returns 'BIGBAG Store' when its argument is not present" do
      expect(full_title).to eq("BIGBAG Store")
    end

    it "returns 'xxx - BIGBAG Store' when its argument is xxx" do
      expect(full_title(page_title: "hoge")).to eq("hoge - BIGBAG Store")
    end
  end
end
