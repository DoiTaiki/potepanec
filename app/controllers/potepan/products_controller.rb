class Potepan::ProductsController < ApplicationController
  COUNT_OF_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: [:default_price, :images]).sample(COUNT_OF_RELATED_PRODUCTS)
  end
end
